# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.


#PSEUDOCODE:
#Input: we have a list of nUMbers that are "strings"
#and they are seperated by commas

#for the output it looks like sums are being added up into
#a total and printed with all of the other values
#it also looks like the numbers are turned into integars
# instead of strings

def add_csv_lines(csv_lines):
    new_csv_line = ["",]
    for item in new_csv_line.split("",):
        if item in new_csv_line:
            print (sum(item))

print (int(add_csv_lines(["3", "1", "9"])))

#FIXING MY CODE WITH SOLUTION GIVEN

def add_csv_lines(csv_lines):
    new_csv_line = []
    for item in csv_lines:
        pieces = item.split(",")
        line_sum = 0
        for piece in pieces:
            value = int(piece)
            line_sum += value
        new_csv_line.append(line_sum)
    return new_csv_line

#Actual answer
# def add_csv_lines(csv_lines):
#     # result_list = new empty list
#     result_list = []                                    # solution
#     # for each item in the csv_lines
#     for item in csv_lines:                              # solution
#         # pieces = split the item on the comma
#         pieces = item.split(",")                        # solution
#         # line_sum = 0
#         line_sum = 0                                    # solution
#         # for each piece in pieces
#         for piece in pieces:                            # solution
#             # value = convert the piece into an int
#             value = int(piece)                          # solution
#             # add the value to sum
#             line_sum += value                           # solution
#         # append sum to the result_list
#         result_list.append(line_sum)                    # solution
#     # return result_list
#     return result_list                                  # solution
#     # pass

def add_csv_lines(csv_lines):
    result_list = []
    for item in csv_lines:
        pieces=item.split(",")
        line_sum = 0
        for piece in pieces:
            value=int(piece)
            line_sum += value
        result_list.append(line_sum)
    return result_list

print (add_csv_lines(["3", "1,9"]))
