# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.


# #ORIGNAL CODE I TRIED
# def pairwise_add(list1, list2):
#     new_list = []
#     for num in list1 and list2:
#         return list1 + list2
#     return new_list

# list1 = [1, 2, 3, 4]
# list2 = [4, 5, 6, 7]



#NEW CODE USING SOLUTION GIVEN
def pairwise_add(list1, list2):
    new_list = []
    for value1, value2 in zip(list1, list2):
        new_list.append(value1 + value2)
    return new_list

list1=[1,2,3,4]
list2=[4,5,6,7]
print (pairwise_add(list1, list2))


# # SOLUTION GIVEN
# def pairwise_add(list1, list2):
#     results = []                                        # solution
#     for value1, value2 in zip(list1, list2):            # solution
#         results.append(value1 + value2)                 # solution
#     return results
