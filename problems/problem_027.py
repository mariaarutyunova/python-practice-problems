# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

#input: list, integars, numbers
#out: if list has numebrs the max number in the list
# if no numebrs return none


my_list = [3,6,1,7]
#iterating over a list
for item in my_list:
    print (item)
num1 = my_list[0]
num2 = my_list[3]
#determing whether num1 is bigger than num2

if num2 < num1:
    print(f"num1: {num1} is bigger than num 2: {num2}")
else:
    print(f"Num2: {num2} is bigger than Num 1: {num1}")


def max_in_list(values):
    if len(values) == 0:
        return None
    #to start off, set max to the first number in the list
    max_value = values [0]
    #go through the list, and update the max
    for num in values:
        if num > max_value:
            max_value = num
    #return the max
    return max_value

print(max_in_list([]))
print(max_in_list([4,6,1,8,34]))
print(max_in_list([-4,-6,-1,-8,-34]))
