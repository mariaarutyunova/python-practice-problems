# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    if attendees_list / members_list >= .5:
        return True
    else:
        return False

attendees_list = 30
members_list = 14
print(has_quorum(members_list, attendees_list))
