# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def minimum_value(value1, value2):
#     minVal = (value1,value2)
#     for i in range(value1, value2):
#         if value1 < value2:
#             return value1
#     else:
#         return value2

# value1 = 2
# value2 = 3


def minimum_value(value1, value2):
    if value1 <= value2:
        return value1
    else:
        return value2

value1=2
value2=10
print(min(value1, value2))
