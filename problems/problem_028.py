# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

#what I found online

def remove_duplicate_letters(str, s):
    s = set()

    for i in str:
        s.add(i)

    st= ""
    for i in s:
        st=st+i
    return st



str = "geeksforgeeks"
n = len(str)
print(remove_duplicate_letters(list(str), n))


#solutions way ->

def remove_duplicate_letters(s):
    result = ""
    for letter in s:
        if letter not in result:
            result = result + letter
    return result


s = "abccbad"
print(remove_duplicate_letters)
