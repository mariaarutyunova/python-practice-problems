# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.


#HOW I TRIED TO DO IT
# def find_indexes(search_list, search_term):
#     new_list = []
#     for value1, value2 in range (len(value1,value2)):
#         new_list.append(value1,value2)
#     for (value1,value2) in enumerate(new_list):
#         return new_list

# search_list = [1, 2, 3, 4, 5]
# search_term = 4


# print(find_indexes(search_list, search_term))



# #NEW CODE USING SOLUTION GIVEN
def find_indexes(search_list,search_term):
    new_list = []
    for index, value in enumerate(search_list):
        if value == search_term:
            new_list.append(index)
    return new_list


search_list = [1, 2, 3, 4, 5]
search_term = 4
print(find_indexes(search_list, search_term))



# # #GIVEN SOLUTION

# def find_indexes(search_list, search_term):
#     results = []                                        # solution
#     for index, value in enumerate(search_list):         # solution
#         if value == search_term:                        # solution
#             results.append(index)                       # solution
#     return results
